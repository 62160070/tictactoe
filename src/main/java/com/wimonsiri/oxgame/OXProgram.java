/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wimonsiri.oxgame;

import java.util.Scanner;

/**
 *
 * @author Acer
 */
public class OXProgram {

    static char winner = '-';
    static boolean isfinish = false;
    static int row, col;
    static Scanner kb = new Scanner(System.in);
    static char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'},};
    static char player = 'X';
    static int turn = 0;

    static void showTable() {
        System.out.println("  1  2  3 ");
        for (int i = 0; i < table.length; i++) {
            System.out.print(i + 1);
            for (int j = 0; j < table[i].length; j++) {
                System.out.print(" " + table[i][j] + " ");
            }
            System.out.println();
        }
    }

    static void showTurn() {
        System.out.println(player + " turn");
    }

    static void input() {
        while (true) {
            System.out.println("Please input Row Col :");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            if (table[row][col] == '-') {
                table[row][col] = player;
                break;
                //
            }
            System.out.println("Error: table at row and col is not empty");
        }

    }

    static void checkcol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isfinish = true;
        winner = player;
    }

    static void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isfinish = true;
        winner = player;
    }

    static void checkX_Left() {
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                if (row == col && table[row][col] != player) {
                    return;
                }
            }
        }
        isfinish = true;
        winner = player;
    }

    static void checkX_Right() {
        int col = 2;
        int row = 0;
        for (int round = 0; round < 3; round++) {
            if (table[row][col] != player) {
                return;
            }
            col--;
            row++;
        }
        isfinish = true;
        winner = player;
    }

    static void checkwin() {
        checkcol();
        checkRow();
        checkX_Left();
        checkX_Right();
        checkDraw();

    }

    static void checkDraw() {
        if (turn == 9 && winner == '-') {
            isfinish = true;
        }

    }

    static void switchPlayer() {
        if (player == 'X') {
            player = 'O';
        } else {
            player = 'X';
        }
    }

    static void showResult() {
        if (winner == '-') {
            System.out.println("Draw!!!");
        } else {
            System.out.println(" " + winner + " Win!!!");
        }
        //System.out.println("I don't known!");
    }

    static void showBye() {
        System.out.println("Bye bye......");
    }

    public static void main(String[] args) {
        System.out.println("Welcome to OX Game");
        do {
            showTable();
            showTurn();
            input();
            checkwin();

            switchPlayer();
            //turn++;
        } while (!isfinish);

        showResult();
        showBye();
    }

}
